import React, {useState, useEffect} from 'react';
import {
  Alert,
  Button,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Axios from 'axios';

const Item = ({nama, alamat, telepon, onPress, onDelete}) => {
  return (
    <View style={styles.itemContainer}>
      <TouchableOpacity onPress={onPress}>
        <Text style={styles.nama}>{nama}</Text>
        <Text style={styles.alamat}>{alamat}</Text>
        <Text style={styles.telepon}>{telepon}</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={onDelete}>
        <Text style={styles.delete}>x</Text>
      </TouchableOpacity>
    </View>
  );
};
const LocalAPI = () => {
  const [nama, setName] = useState('');
  const [alamat, setAlamat] = useState('');
  const [telepon, setTelepon] = useState('');
  const [users, setUsers] = useState([]);
  const [button, setButton] = useState('Simpan');
  const [selectUser, setSelectUser] = useState({});
  useEffect(() => {
    getData();
  }, []);
  const submit = () => {
    const data = {
      nama,
      alamat,
      telepon,
    };
    if (button === 'Simpan') {
      //console.log('data before send', data);
      Axios.post('http://10.0.2.2:5050/users', data).then((res) => {
        console.log('res : ', res);
        setName('');
        setAlamat('');
        setTelepon('');
        getData();
      });
    } else if (button === 'Update') {
      Axios.put(`http://10.0.2.2:5050/users/${selectUser._id}`, data).then(
        (res) => {
          //console.log('res update' , res)

          setName('');
          setAlamat('');
          setTelepon('');
          getData();
          setButton('Simpan');
        },
      );
    }
  };
  const getData = () => {
    Axios.get('http://10.0.2.2:5050/users').then((res) => {
      //console.log('res :', res);
      setUsers(res.data);
    });
  };
  const selectItem = (item) => {
    console.log('select ', item);
    setSelectUser(item);
    setName(item.nama);
    setAlamat(item.alamat);
    setTelepon(item.telepon);
    setButton('Update');
  };
  const deleteItem = (item) => {
    //console.log(item);
    Axios.delete(`http://10.0.2.2:5050/users/${item._id}`).then((res) => {
      console.log('res delete :', res);
      getData();
    });
  };
  return (
    <View style={styles.container}>
      <Text style={styles.line}>Local API</Text>
      <Text>Masukkan angota</Text>
      <TextInput
        style={styles.input}
        placeholder="Nama"
        value={nama}
        onChangeText={(value) => setName(value)}
      />
      <TextInput
        style={styles.input}
        placeholder="alamat"
        value={alamat}
        onChangeText={(value) => setAlamat(value)}
      />
      <TextInput
        style={styles.input}
        placeholder="no telepon"
        value={telepon}
        onChangeText={(value) => setTelepon(value)}
      />
      <Button title={button} onPress={submit} />
      <View style={styles.line} />
      {users.map((user) => {
        return (
          <Item
            key={user._id}
            nama={user.nama}
            alamat={user.alamat}
            telepon={user.telepon}
            onPress={() => selectItem(user)}
            onDelete={() =>
              Alert.alert('Peringatan', 'Anda yakin akan menghapus ?', [
                {text: 'Tidak', onPress: () => console.log('button tidak')},
                {text: 'Ya', onPress: () => deleteItem(user)},
              ])
            }
          />
        );
      })}
    </View>
  );
};
export default LocalAPI;
const styles = StyleSheet.create({
  container: {padding: 20},
  textTitle: {textAlign: 'center', marginBottom: 20},
  line: {height: 2, backgroundColor: 'black', marginVertical: 20},
  input: {
    borderWidth: 1,
    marginBottom: 12,
    borderRadius: 25,
    paddingHorizontal: 18,
  },
  itemContainer: {flexDirection: 'row', marginBottom: 20},
  nama: {fontSize: 20, fontWeight: 'bold'},
  alamat: {fontSize: 16},
  telepon: {fontSize: 12, marginTop: 8},
  delete: {fontSize: 20, fontWeight: 'bold', color: 'red'},
});
