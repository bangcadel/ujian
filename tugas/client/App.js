/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {View} from 'react-native';
import LocalAPI from './src';

export default function App() {
  return (
    <View>
      <LocalAPI />
    </View>
  );
}
