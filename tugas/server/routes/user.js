const express = require('express');
const router = express.Router();
const User = require('../model/User');

//create
router.post('/' , async (req, res) => {
    const usersPost = new User({
        nama : req.body.nama,
        alamat : req.body.alamat,
        telepon : req.body.telepon
    });

    try {
        const user = await usersPost.save()
        res.json(user)
    } catch (error) {
        res.json({ message : err})
    }
});
// router.get('/:name', async (req,res) => {
//     try {
//         const detail = await User.findOne()
//     } catch(error) {

//     }
// })

//read
router.get('/', async (req,res) => {
    try {
        const  users = await User.find();
        res.json(users);
    } catch (error) {
        res.json({message : err})
    }
});

//update 
router.put('/:usersId', async (req,res) => {
    try {
        const userUpdate = await User.updateOne({ _id: req.params.usersId}, {
            nama : req.body.nama,
            alamat : req.body.alamat,
            telepon : req.body.telepon
        });
        res.json(userUpdate);
    } catch (err) {
        res.json({message: err})
    }
});
//delete
router.delete('/:usersId', async (req,res) => {
    try {
        const usersDelete = await User.deleteOne({ _id : req.params.usersId})
        res.json(usersDelete);
        
    } catch (error) {
        res.json({ message: error})
    }
})


module.exports = router;