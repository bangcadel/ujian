const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv/config');
const cors = require('cors');
const userRoutes = require('./routes/user');

//middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended : true }));
app.use(cors());
//routes endpoint
app.use('/users', userRoutes);
//koneksi mongose
mongoose.connect( process.env.DB_CONNECTION, { useUnifiedTopology: true,  useNewUrlParser: true });
let db = mongoose.connection;
db.on('error' , console.error.bind( console, 'Database connect Error'));
db.once( 'open', () => {
    console.log('Database is connected');
})


app.listen(6969, () => {
    console.log('server run di 5050')
})