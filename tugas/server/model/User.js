const mongoose = require('mongoose');
const UserSchema = mongoose.Schema({
    nama: {
        type : String,
        require : true
    },
    alamat : {
        type : String,
        require : true
    },
    telepon : {
        type : String,
        require : true
    }
})

module.exports = mongoose.model('User', UserSchema);